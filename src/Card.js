import React from 'react';



// class Card extends React.Component{
//     render(){
//         return (
//             <div>
//             <img alt = 'Robots' src='' />
//             <div>
//                 <h2>Jane Doe</h2>
//                 <p>jane.doe@gmail.com</p>
//             </div>
//            </div>)
       
        
//     }
    
// }
const Card = ({name,email,id}) =>{
   
    return (
        
        <div  className='tc bg-light-green dib br3 pa3 ma2 grow bw2 shadow-2'>
            <img alt='robots' src={`https://robohash.org/ ${id}?200x200`}/>
            <div>
                <h2>{name}</h2>
                <p>{email}</p>
            </div>
        </div>);
}

export default Card;