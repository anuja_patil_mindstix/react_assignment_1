import React,{Component} from 'react';
import CardList from '../src/CardList';
import Searchbox from '../src/Searchbox';
import {robots} from '../src/robots';
import Scroll from '../src/Scroll';

class App extends Component{
    constructor(){
        super()
        this.state = {
            
                robots: [],
                searchfield: ''
            
        }
        
    }
    componentDidMount(){
        fetch('https://jsonplaceholder.typicode.com/users').then(response=>{
            return response.json();
        }).then(users=>{
            this.setState({robots:robots});
        })
       
       
    }
    onSearchChange = (event) => {
        this.setState({ searchfield: event.target.value})
       
    }
    render(){
        const filteredRobots = this.state.robots.filter(robot=>{
            return robot.name.toLowerCase().includes(this.state.searchfield.toLowerCase());
        })
        
        return ( 
            <div className='tc'>
             <h1>RoboFriends</h1>
             <Searchbox searchChange = {this.onSearchChange}/>
             <Scroll>
             <CardList robots = {filteredRobots}/>
             </Scroll>
             
            </div>
            
            );
    }
    
}

export default App;