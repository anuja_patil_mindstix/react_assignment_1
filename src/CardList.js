import React from 'react';
import Card from '../src/Card.js';

const CardList = ({robots}) =>{
    //Use map to return multiple elements
   
    return(
        <div>
        {robots.map((user,i)=>{
        return( <Card key={i} 
        id={robots[i].id} 
        name={robots[i].name} 
        email={robots[i].email}/>
        )
    })}
        </div>
    );
}

export default CardList;